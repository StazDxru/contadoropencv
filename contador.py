
import argparse
import datetime
import imutils
import math
import cv2
import numpy as np

width = 800

textIn = 0
textOut = 0

# Linea de Entrada
def testIntersectionIn(x, y):

    res = -450 * x + 400 * y + 157500
    if((res >= -550) and  (res < 550)):
        print (str(res))
        return True
    return False


# Linea de Salida
def testIntersectionOut(x, y):
    res = -450 * x + 400 * y + 180000
    if ((res >= -550) and (res <= 550)):
        print (str(res))
        return True

    return False

# Video a evaluar
camera = cv2.VideoCapture("video.mp4")

# Declaracion de variable Primer Frame
firstFrame = None

# Bucle para cada frame del video
while True:
    # Coge el frame actual, listo para evaluar
    (grabbed, frame) = camera.read()
    text = "Unoccupied"

    # Si el frame no se puede reconocer, paramos y seguimos con el siguiente
    if not grabbed:
        break

    # Redimensionamos el frame, convertimos a escala de grises y desenfocamos
    frame = imutils.resize(frame, width=width)
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (21, 21), 0)

    # Si el Primer Frame es None, lo inicializa
    if firstFrame is None:
        firstFrame = gray
        continue

    # Calcula la diferencia absoluta entre el Frame actual y el Primer Frame
    frameDelta = cv2.absdiff(firstFrame, gray)
    thresh = cv2.threshold(frameDelta, 25, 255, cv2.THRESH_BINARY)[1]
    # Dilata el umbral de la imagen para llenar los agujeros luego encuentra los contornos
    thresh = cv2.dilate(thresh, None, iterations=2)
    _, cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # Hace un loop en los contornos
    for c in cnts:
        # Si el contorno es muy pequenio, lo ignora
        if cv2.contourArea(c) < 12000:
            continue
        # Calcula la caja delimitadora para el contorno, dibuja en el marco y actualiza el texto
        (x, y, w, h) = cv2.boundingRect(c)
        cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

        cv2.line(frame, (width / 2, 0), (width, 450), (250, 0, 1), 2) #Linea azul
        cv2.line(frame, (width / 2 - 50, 0), (width - 50, 450), (0, 0, 255), 2)#Linea roja


        rectagleCenterPont = ((x + x + w) /2, (y + y + h) /2)
        cv2.circle(frame, rectagleCenterPont, 1, (0, 0, 255), 5)

        if(testIntersectionIn((x + x + w) / 2, (y + y + h) / 2)):
            textIn += 1

        if(testIntersectionOut((x + x + w) / 2, (y + y + h) / 2)):
            textOut += 1

        # Dibuja el texto y la fecha en el Frame

        # Muestra el marco y graba si el usuario pulsa una tecla
        # cv2.imshow("Thresh", thresh)
        # cv2.imshow("Frame Delta", frameDelta)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    cv2.putText(frame, "In: {}".format(str(textIn)), (10, 50),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, "Out: {}".format(str(textOut)), (10, 70),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
    cv2.putText(frame, datetime.datetime.now().strftime("%A %d %B %Y %I:%M:%S%p"),
                (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1)
    cv2.imshow("Security Feed", frame)


# Limpia la camara y cierra las ventanas abiertas
camera.release()
cv2.destroyAllWindows()