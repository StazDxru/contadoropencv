#Contador de personas (IN/OUT)

Comandos a correr: (Tener instalado Python 2.7 y pip)

```sh
$ pip install datetime
$ pip install imutils
$ pip install opencv-python
```
   
Para correr el programa, solo ejecutar: 
  
```sh
$ python contador.py
```
   
Cambiar esta línea de código para probar otro video:

`camera = cv2.VideoCapture("video.mp4")`

Code 4 Fun!